\setcounter{figure}{0}
\setcounter{Listing}{0}
\setcounter{table}{0}
\setcounter{Equation}{0}

\chapter{Deep neural networks}
Deep neural networks are a composition of simpler building blocks. In this section we let go a bit of the formal guarantees to steer towards approaches which have been shown to work.

With these we try to learn an unknown function $\psi : \Rn \mapsto \mathbb{R}^k$ with $k \in \mathbb{N}$. 

With supervised, a set of labelled data ${(\bx_t, \by_t)}$ is provided.

Deep learning is an approach where $\psi$ is a composition of functions $\psi = g \circ H_L \circ H_{L-1} \circ \dots \circ H_L$, each corresponding to a layer in the network.

The partial maps introduce intermediary representations of the data. Deep neural networks combine depth and expansion (in the intermediate layer dimensionality).

\section{Layer maps}
Here we investigate the different possibilities we have for the definitions of the simpler layer components. These can then be chained in order to increase the model expressiveness.

\subsection{Linear map}
Perhaps the simplest layer that can be conceived of is a linear map. Here $F: \Rn \mapsto \Rm$ is represented by a matrix $\Rnm$.
In other words $F(\bx) = \bThet\bx$. The class of linear functions is however closed under composition as $(F_1 \circ F_2)(\bx) = \bThet_1\bThet_2\bx$.

This closure property means there is nothing to be gained by composing linear map layer in terms of modelling power.

\subsection{Non-linear layers}

A simple way to fix this closure problem is to introduce a non-linear function $\phi : \R \mapsto \R$, called activation, which is applied to each entry of the output vector of the linear map. The non-linear function $\bPhi : \Rm \mapsto \Rm$ simply denotes this entry-wise application. Activations are very usually parameter-less. In order to be fully expressive it is important for the activation not to be polynomials because they are also closed under composition.

The rectified linear unit (ReLU) function ($\phi_{\text{ReLU}}(x) = \max\{x, 0\}$) is an example of such an activation function. Other choices are often the hyperbolic tangent or the sigmoid function

The overall the layer function $H : \Rn \mapsto \Rm$ is $H = \bPhi \circ F$ i.e. $H(\bx; \bThet) = \bPhi(\bThet\bx)$.

\subsection{Multi layer perceptron (MLP)}

The multi layer perceptron is a depth $L$ composition of sigmoid network with a linear map at the output. i.e. $\by = \bThet_{L}\sigma(\bThet_{L-1} \dots (\sigma(\bThet_1\bx))\dots)$.

The single layer version of the MLP is called the perceptron and can be quite easily explicitly stated as shown in Equation \ref{eq:4-perceptron-explicit}. A graphical representation of the perceptron is shown in Figure \ref{fig:4-perceptron} 

\begin{Equation}[H]
  \input{res/4-dnn/eqn/perceptron.tex}
  \caption{Explicit statement of the perceptron.}
  \label{eq:4-perceptron-explicit}
\end{Equation}

\begin{figure}[H]
  \centering
  \resizebox{0.27\textheight}{!}{
    \input{res/4-dnn/tikz/perceptron.tex}
  }
  \caption{Graphical representation of the perceptron.}
  \label{fig:4-perceptron}
\end{figure}

Using the squared loss between the output of the perceptron and the label, the derivatives of the loss w.r.t the entries of $\bThet$ and $\bbeta$ are shown in Equation \ref{eq:4-perceptron-lossder}.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/perceptron-lossder.tex}
  \caption{Derivative of the squared loss for the perceptron.}
  \label{eq:4-perceptron-lossder}
\end{Equation}

Given these gradients, we can then simply apply (stochastic) gradient descent to optimize $\bThet$ and $\bbeta$. With stochastic gradient descent, the gradient is only computed with on a minibatch $\mathcal{S}_t$ which is simply a randomly sampled subset of the training data $\mathcal{S}$. At each iteration, the minibatch is sampled again from the whole dataset. 

This practice saves some computational cost while still allowing for slower convergence. When considering more data points, the gradient simply a sum over $(\bx,y) \in \mathcal{S}_t$ of the derivatives shown in Equation \ref{eq:4-perceptron-lossder}. The update to the parameters is made by subtracting the weighted gradient of the parameters from the current parameters. The weight is often denoted $\eta$ and is called the learning rate.

\section{Back propagation}

For MLPs with more than one layer, the formulation of the gradient is quite hard to explicitly derived. Instead, we define it from the last to the first layer, expressing the gradient at each step based on the deeper layers. This practice is justified because, as far as the parameters in layer $i$ are concerned, the parameters in previous layers do not impact the gradient of the parameters and the gradient only depends on the gradient of the further layers, not on their explicit structure. With back propagation, the gradient is also compositionnally defined and makes heavy use of the chain rule for derivation.

For a single unit $h_{li}$ at depth $l$ with parameters $\bthet_{li}$, $h_{li}(\bz) = \phi(\inprod{\bthet_{li}}{\bz})$ Assuming we can compute the partial derivative $\delta_{li}$ of the loss function $\ell$ with respect to $h_{li}$ is also called the delta for unit or the error signal of the unit. The derivative of the activation is called the sensitivity of the unit. Thus, the parameter gradient $\nabla_{\bthet}(\bz; \bthet) = \delta \nabla_{\bthet}h(\bz; \bthet) = \delta \phi'(\inprod{\bthet}{\bz})\bz$. This way of expressing the gradient w.r.t the parameters show the gradient is always pointing in the direction of the input of the layer.

Now if the forward pass (i.e. application of the model on an input), if we keep around the inputs to the layers, we can start computing the gradients backwards from the end of the model and locally perform the necessary parameter updates.

In order to rigorously define backwards propagation we make use of the Jacobi. The Jacobi $\bJ_{H}$ of a function $H : \Rn \mapsto \Rm$ is a $\Rnm$ matrix valued function whose $ij$-th entry is the partial derivative of $h_i$ with respect to $z_i$. The Jacobi is the vector valued - vector parametrized function version of the derivative. 

The error signal for the unit at layer $k$, $\bdelta_k$ can be defined in terms of the Jacobi matrices of the next layers until the output of the model using the chain rule as shown in Equation \ref{eq:4-mlp-delta}.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-delta.tex}
  \caption{Error signal definition in terms of the next layer derivatives.}
  \label{eq:4-mlp-delta}
\end{Equation}

Specific instances of activation correspond to different Jacobi matrices of the maps $H(\bx) = \phi(\bThet\bx)$. 
\begin{itemize}
  \item The Jacobi a linearly activated map is simply $\bThet$.
  \item The Jacobi of a ReLU activated map is $\text{diag}(\bfancyx)\bThet$, where $\bfancyx \in \{0,1\}^m$ and $\bfancyx = \mathbb{I}[\bThet\bx > 0]$ (i.e. it indicates whether the entry of the vector-matrix product is positive).
  \item The Jacobi of a sigmoid activated map is $\text{diag}(\bfancyx)\bThet$, but here, $\bfancyx \in [0,1]^m$ and $\bfancyx = \sigma'(\bThet\bx)$.
\end{itemize}

\subsection{Initial error signal}

The initial error signal depends on the loss function used to evaluate the output of the model. Equation \ref{eq:4-mlp-init-errsig}shows the error signals for the squared loss (used for regression) and the logistic loss (used for binary classification).

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-init-errsig.tex}
  \caption{Initial error signals for squared and logistic loss.}
  \label{eq:4-mlp-init-errsig}
\end{Equation}

\section{Gradient methods}

Now that we showed how to retrieve the gradients, is using them for gradient a way to optimize our problem ? In general the models given by MLPs are highly non-convex, so there are no guarantees a global minimum will be found, however after some time, the models ends up in a convex region of the space where a local minimum is reached after some iterations.

Once a relatively convex region is reached, we can approximate it using Taylor's expansion for instance and reformulate the objective function into a convex quadratic objective as shown in Equation \ref{eq:4-mlp-quad-obj}.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-quad-obj.tex}
  \caption{Convex quadratic objective function.}
  \label{eq:4-mlp-quad-obj}
\end{Equation}

In the convex quadratic objective function, $\bQ$ is positive definite, so it can be diagonalized into $\bQ = \bU\bLam\bU^\top$, changing the basis, with $\bthet \rightarrow \bU^\top\bthet$ and $\bq \leftarrow \bU^\top\bq$ leads to a separable problem shown in Equation \ref{eq:4-mlp-sep-obj}.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-sep-obj.tex}
  \caption{Separable problem for convex quadratic objective using change of basis.}
  \label{eq:4-mlp-sep-obj}
\end{Equation}

Taking the gradient $\ell_i'(\vartheta) = \lambda_i\vartheta - q_i$ of the separated problem yields the optimality conditions shown in Equation \ref{eq:4-mlp-opt-cond}. It also shows how it is possible to shift $\ell_i$ so its minimum is at 0.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-opt-cond.tex}
  \caption{Optimality conditions (i.e. assuming $\ell_i(\vartheta) = 0$).}
  \label{eq:4-mlp-opt-cond}
\end{Equation}

Using this formulation of the approximation of locally convex objective, Equation \ref{eq:4-mlp-grad-step} shows the result of performing one gradient descent step. This means the sufficient condition for loss decrease is $\eta < 2/\lambda_i \ \forall\ i$. Thus $\eta < 2/ \lambda_{\max}$. This condition explicitly states why the learning rate should not be too large, because in the direction, of the largest eigenvalue of the quadratic convex approximation of the objective the gradient descent would oscillate around the minimum. 

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-grad-step.tex}
  \caption{Gradient descent step on the approximation.}
  \label{eq:4-mlp-grad-step}.
\end{Equation}

In the direction of the lowest eigenvalue however, the progress is very slow, thus we would like the largest step size as possible. Both these constraints on the learning rate $\eta$ are shown in Equation \ref{eq:4-mlp-best-lr}. It also shows the resulting rate $\rho$ expressed in terms of the condition number $\kappa$.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-best-lr.tex}
  \caption{Learning rate bounds.}
  \label{eq:4-mlp-best-lr}.
\end{Equation}

This condition on the ratio between the largest and smallest eigenvalue shows the problem of the optimal value for $\eta$ is quite complicated to solve if $\eta$ is a single parameter for all directions. Indeed, if $\lambda_{\text{min}} << \lambda_{\text{max}}$, the step size must be really small, so the steps do not overshoot, but the process is thus really slow for the other direction where a long valley needs to be traversed.

\subsection{Smoothness of the objective}

Another condition for the gradient descent method to be useful is for the objective function to be smooth. Smoothness is defined by the Lipschitz continuity of the gradient of the objective $\nabla \ell$. The definition is shown in Equation \ref{eq:4-mlp-smooth}.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/mlp-smooth.tex}
  \caption{Smoothness definition.}
  \label{eq:4-mlp-smooth}
\end{Equation}

Assuming the gradient of $\ell$ is smooth, we can use the definition of smoothness with the second order Taylor expansion in order to derive another measure of the progress made in one step and to condition it on properties of the function. Equation \ref{eq:4-smoothness-implications} shows the bound on the progress made at each gradient descent step. This shows quite explicitly the progress is achievable since the bound is negative, but also that it is proportional to the gradient size, but also to the inverse of the $L$ smoothness which is small when the function is smooth.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/smoothness-implications.tex}
  \caption{Progress bound at gradient descent step.}
  \label{eq:4-smoothness-implications}
\end{Equation}

\subsection{Gradient norm}
Another factor determining the ability of the gradient descent to make progress towards the objective is the magnitude of the gradient. Since the step size is proportional to the gradient, when the gradient vanishes at an extreme or at a saddle point, the steps become increasingly small. 

Thus, instead of directly addressing whether the convergence stops exactly at the minimum, we can reformulate the question as asking whether the gradient becomes small enough at some point. For $\ell$ differentiable at $\bthet$, $\bthet$ is an $epsilon$-critical point if $\Vert\nabla \ell\Vert \leq \epsilon$.

It can be shown that gradient descent on a $L$-smooth differentiable function $\ell$ with step size $\eta = 1/L$ finds an $\epsilon$-critical point in at most $k = 2L(\ell(\bthet^0) - \ell^\ast)/\epsilon^2$ steps, with $\ell^\ast = \min_{\bthet}\ell(\bthet)$. Thus given $\ell$ is smooth enough, gradient descent is shown to converge in $\bigo(\epsilon^{-2})$

\section{Fast convergence to local minimum}

Using the Polyak-Łojasiewicz (PL-)condition defined in Equation \ref{eq:4-pl}, we can reach stronger convergence time guarantees.

\begin{Equation}[H]
  \input{res/4-dnn/eqn/pl.tex}
  \caption{Polyak-Łojasiewicz condition.}
  \label{eq:4-pl}
\end{Equation}

The convergence guarantee is given by the following theorem: For $\ell$ a differentiable, $L$-smooth and $\mu$ PL-condition conforming, the gradient descent with step size $\eta = \frac{1}{L}$ converges at a geometric rate shown in Equation \ref{eq:4-pl-conv}.
\begin{Equation}[H]
  \input{res/4-dnn/eqn/pl-conv.tex}
  \caption{Geometric convergence rate.}
  \label{eq:4-pl-conv}
\end{Equation}

\subsection{Momentum and acceleration methods}

Up until now, the solutions and their guarantees only function in settings where the objective is convex. We however need more general solutions and approaches. 

A relatively easy solution to overcome saddle points for instance are noisy gradients, obtained either by adding noise or simply by performing SGD which introduces noise in the gradient as part of the process.

\subsection{Heavy ball method}
The idea of this method is to carry over the history of previous updates at each update step as shown in Equation \ref{eq:4-hb}.
\begin{Equation}[H]
  \input{res/4-dnn/eqn/hb.tex}
  \caption{Change to the gradient descent with the heavy ball method.}
  \label{eq:4-hb}
\end{Equation}