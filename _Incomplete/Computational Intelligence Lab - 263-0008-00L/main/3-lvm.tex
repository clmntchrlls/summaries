\setcounter{figure}{0}
\setcounter{Listing}{0}
\setcounter{table}{0}
\setcounter{Equation}{0}

\chapter{Latent variable models}

Latent variable models are models where the data is modelled with a probability distribution parametrized by latent, unobservable variables. In mathematical terms, The data $X = (X_1, X_2, \dots, X_n)$ is augmented by latent variables $Z = (Z_1, Z_2, \dots Z_m)$ for which we specify $P(X,Z)$ and with which we can derive $P(X) = \sum_{Z} P(X,Z)$.

\section{Mixture models}
One of the simplest latent variable models is the Mixture Model (MM). In those, a set of data-points $X = \{\bx_t \in \Rn : t = 1,2,\dots, s\}$ are to be categorized in $k$ different classes. A categorical variable $Z_t$ is assigned for each of the $k$ classes. This model is a probabilistic clustering model.

Each latent class variable follows a categorical law $Z_t \iid \text{Categ}(\pi_1, \dots, \pi_k)$ with $P(Z_t = z) = \pi_z$. $\bpi = (\pi_1, \dots, \pi_k)$ are the prior probabilities for each class. We often do not know what these categories mean a priori, but the goal is often to discover these via clustering.

The complete data model is constructed by defining the conditional data distribution $P(\bx \vert z)$ for each class. This is performed by choosing a single suitable data distribution for all classes and assigning a set of parameter $\bthet_z$ for each component thus $P(\bx \vert z) = P(\bx ; \bthet_z)$. The complete data model is shown in Equation \ref{eq:3-mm-data-model}.

A common choice for the distribution is a Gaussian distribution. In that case, the model is called a Gaussian mixture model (GMM).

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-data-model.tex}
  \caption{Complete data model for mixture models.}
  \label{eq:3-mm-data-model}
\end{Equation}

The total data point probability is then achieved using marginalization as shown in Equation \ref{eq:3-mm-marg}. It is important to note here that $\bpi > \bzer$ and $\sum_{i} \pi_i = 1$, so MMs are convex combinations of distributions. 

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-marg.tex}
  \caption{Marginalization over categories yielding total data point probability.}
  \label{eq:3-mm-marg}
\end{Equation}

\subsection{Clustering and posteriors}

The posteriors can be derived using Bayes' rule as shown in Equation \ref{eq:3-mm-post}. These represent the probabilistic clustering of the data points.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-post.tex}
  \caption{Category posteriors.}
  \label{eq:3-mm-post}
\end{Equation}


\subsection{Parameter estimation: maximum likelihood estimation (MLE)}

Now that the model is formulated, what is left is to find the parameters $\bpi$ and $\bthet$. In order to do so we use maximum log-likelihood. A likelihood is a probability density function in which the roles are reversed. 

The log-likelihood is equivalent to the likelihood, but it is more convenient to work with since the properties of the log function transforms the products into sums which are easier to manage computationally. Given the data, treated as fixed, the goal is to find the best values of the parameters for our problem. 

The general formula for the log-likelihood $\ell(\bthet, \bpi; X)$ is shown in Equation \ref{eq:3-mm-ll}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-ll.tex}
  \caption{Log likelihood for MM.}
  \label{eq:3-mm-ll}
\end{Equation}

\subsection{Expectation maximization (EM) algorithm}

There is unfortunately no closed form solution for the parameter estimation, but, fortunately the expectation maximization (EM) algorithm provides us a computational way to derive a good choice of parameter values. 

The core property this algorithm exploits is Jensen's inequality which states that for every concave function $\phi$ and every convex function $\psi$, $\phi(\E[X]) \geq \E[\phi(X)]$ and $\psi(\E[X]) \leq \E[\psi(X)]$. Using this inequality, it is possible to define a variational bound using complicated math which yields the evidence lower bound (ELBO) shown in Equation \ref{eq:3-mm-var-bnd}. This step is extremely useful because it is complicated to optimize a log of a sum, and this bound allows for the transformation of this log of sum into a sum of logs.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-var-bnd.tex}
  \caption{Evidence lower bound for MLE.}
  \label{eq:3-mm-var-bnd}
\end{Equation}

ELBO is then set as the objective to maximize. Optimizing w.r.t the variational parameters $q_{tz}$ can be done per data point (which is handy) and improves the tightness of the bound. Whereas optimizing w.r.t $\bthet$ and $\bpi$ improves the model fit.

The EM algorithm alternates between optimizing for the variational parameters in the E-step and optimizing w.r.t the model parameters in the M-step

\subsubsection{E-step}
In the E-step, the algorithm solves for optimal variational parameters with the model parameters treated as fixed. 

In order to find these, Lagrange multipliers are used. The variational parameters are optimized on the constraint that $\sum_{z=1}^k q_{tz} = 1$ and the optimization can be performed for each data point separately. The Lagrangian $\mathcal{L}_t$ for data point $t$ is shown in Equation \ref{eq:3-mm-estep-lagrangian}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-estep-lagrangian.tex}
  \caption{Lagrangian for optimizing the variational parameters $q_{tz}$ for data point $t$.}
  \label{eq:3-mm-estep-lagrangian}
\end{Equation}

The first order optimality condition ($\nabla_{\bq_t} \mathcal{L}_t := 0$) yields Equation \ref{eq:3-mm-estep-fo}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-estep-fo.tex}
  \caption{First order optimality condition for $\bq_{t}$.}
  \label{eq:3-mm-estep-fo}
\end{Equation}

Solving for $\lambda$ could be done manually, but we can also notice $q_{tz} \propto \pi_zP(x_t; \bthet_z)$. We can thus directly identify the value of $q_{tz}$ as the posterior of the latent variable as shown in Equation \ref{eq:3-mm-qtz-is-post}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-qtz-is-post.tex}
  \caption{Explicit $q_{tz}$ definition.}
  \label{eq:3-mm-qtz-is-post}
\end{Equation}

Thus, with $\bthet$ and $\bpi$ fixes, the estimation of the lower bound can be directly computed from the distribution. 

\subsubsection{M-step}

Once the bound is tightened, the goal now is to optimize ELBO with respect to the model parameters. In order to do so, we look separately at the class prior probabilities $\bpi$, then at the class specific parameters $\{\bthet_1, \dots, \bthet_k\}$.

Using Lagrange multipliers we first derive the optimal $\pi_z$ conditioned again on $\sum_{\zeta = 1}^{k} \pi_\zeta = 1$. The first order optimality condition results in Equation \ref{eq:3-mm-mstep-opt-pi}. $\bpi$ is a measure for the relative sizes of the clusters. It is the overall measure of the category occurrence.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-mstep-opt-pi.tex}
  \caption{First order optimality condition for $\pi_{z}$.}
  \label{eq:3-mm-mstep-opt-pi}
\end{Equation}

The class conditionals parameters are the only thing left to optimize $\bthet_z$. The maximization problem is shown in Equation \ref{eq:mm-class-cond-max} is a weighted iid log likelihood, can be maximized for each $z \in \{1,\dots,k\}$ independently, and is tractable if the MLE component of it is tractable.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/mm-class-cond-max.tex}
  \caption{Class conditionals $\bthet_z$ optimization problem.}
  \label{eq:mm-class-cond-max}
\end{Equation}


\subsection{Gaussian mixture models}

For real valued data, a popular choice of distribution for the modelling is the Gaussian distribution because of the central limit theorem. It is often fair to assume a variable found in nature to be Gaussian if it is a sum of iid random variables. The mixture model is then a Gaussian mixture model (GMM).

Settling for the Gaussian distribution in $d$ dimensions, its probability density function is shown in Equation \ref{eq:3-gauss-pdf}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/gauss-pdf.tex}
  \caption{Multivariate Gaussian pdf.}
  \label{eq:3-gauss-pdf}
\end{Equation}

For GMM the EM algorithm functions as shown in Equation \ref{eq:3-gmm-e} for the E step of the algorithm and as in Equation \ref{eq:3-gmm-m} for the optimal derivation of $\bpi$ and $\bthet = \{\{\bmu_z\}_{z = 1}^k, \{\bSig_z\}_{z=1}^k\}$ in the M step.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/gmm-e.tex}
  \caption{E step for GMMs.}
  \label{eq:3-gmm-e}
\end{Equation}

This step makes it obvious that GMMs are an extension of the $k$-means clustering algorithms because the membership of a data point to a cluster depends on the distance of the data point to the center of the cluster. This extension also involves a variance-covariance matrix, of course, which allows for more flexibility that just considering the distance to the cluster center.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/gmm-m.tex}
  \caption{M step for GMMs.}
  \label{eq:3-gmm-m}
\end{Equation}


\section{Topic Models}
The problem of topic models is formulated for a set of documents $D = \{d_1, \dots, d_n\}$ each document $d_i$ has a length $s_i$. The documents are modelled by a field of random variables $\{X_{it}\}_{i \in \{1,\dots n\}, t \in \{1, \dots, s_i\}}$. The realizations $x_{it}$ of each of $X_{it}$ are from a vocabulary $\Sigma$ of $m$ different words.

Topic models try to infer the main different topics in a set of documents and tries to assign each of them to a weighted combination of topics. In order to do so, the topics are characterized by the number of occurrences of specific words within each of the documents, so the order the words appear in does not matter in this context. More technically, this technically means the joint distribution is invariant under permutations. This property is called exchangeability and does not imply independence.

This property implies a sufficient statistic is the number of occurrence of each word within each document. In order to model this, the word occurrence (or term-document) matrix $N \in \mathbb{Z}_{\geq 0}^{n \times m}$ is defined in Equation \ref{eq:3-tm-occ-mtx}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/tm-occ-mtx.tex}
  \caption{Definition of the word occurrence matrix.}
  \label{eq:3-tm-occ-mtx}
\end{Equation}

Topics are defined to be the categories documents fit more or less fit into. Formally they are defined by variables $Z \in \{1, \dots, k\}$. Now instead of attaching a topic to each document, as with the mixture models previously explained, we attach probabilities of words occurrences in each of these topics, so documents can be modelled as being part of a combination of different topics. 

Formally the complete data model is $(X_{it}, Z_{it})$. Documents are characterized by distributions over topics $P(z \vert d)$ and topics as distributions over words $P(w \vert z)$. 

The log likelihood of this model is shown in Equation \ref{eq:3-tm-ll}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/tm-ll.tex}
  \caption{Log likelihood for topic models.}
  \label{eq:3-tm-ll}
\end{Equation}

\subsection{Latent Dirichlet allocation (LDA)}
An extension of the model seen so far that accounts for new yet unseen documents and allows for the generation of new documents is latent Dirichlet allocation (LDA).

We first introduce a parametrized notation for the distribution of the words given the topics and for the distribution of topics given the document as shown in Equation \ref{eq:3-lda-param}. This parametrization allows us to represent the distributions of words given the topics in a matrix $\bU \in \Rmk$ and with a matrix $\bV \in \Rkn$ for the distribution of topics given the document.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/lda-param.tex}
  \caption{Parametrization of distributions.}
  \label{eq:3-lda-param}
\end{Equation}

Since the entries of the matrix are probabilities, the restrictions shown in Equation \ref{eq:3-lda-restr-param} apply to the column vectors $\bu_z$ of $\bU$ and $\bv_i$ of $\bV$.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/lda-restr-param.tex}
  \caption{Restrictions on matrices formed by the parametrization.}
  \label{eq:3-lda-restr-param}
\end{Equation}

A sensible choice for a distribution for a vector of mixing proportions (here the topics in a document $i$) is the Dirichlet distribution, because of the conjugacy principle. This principle states the family of the posterior obtained by Bayes's rule is of the same family as the prior. The Dirichlet distribution is shown in Equation \ref{eq:3-lda-dirichlet}. In this formula, the proportionality factor is not explicitly stated because it is quite complicated. The choices of $\alpha_z$ are also often fixed to a single $\alpha$ in practice which is considered as a hyperparameter to be optimized on held-out data.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/lda-dirichlet.tex}
  \caption{Dirichlet distribution.}
  \label{eq:3-lda-dirichlet}
\end{Equation}

The full Bayesian model for LDA is shown in Equation \ref{eq:3-lda-bayes-mod}. The topic model is augmented with a Dirichlet prior. 

\begin{Equation}[H]
  \input{res/3-lvm/eqn/lda-bayes-mod.tex}
  \caption{Bayesian model for LDA.}
  \label{eq:3-lda-bayes-mod}
\end{Equation}

Using this model, the log-likelihood can again be computed and given $\bU$, the best parameters for the model can be found. There are highly scalable algorithms for solving this problem among which variational-EM, and Markov chain Monte Carlo.

\section{Probabilistic matrix decomposition}

Matrix decomposition was already explored in quite some details previously, but the approach used often stems from the type of problem which is to be solved. In the current case, the problem is more specifically defined since the constraints on the rows of the factors must be probability distributions, not any random vectors.

Considering $\hat{\bN} = \bU\bV$  as defined in Equation \ref{eq:3-lda-param} with $\text{rank}(\hat{\bN}) = k$, $\hat{\bN}$ is not a count matrix but a frequency matrix. i.e. $\hat{N}_{ij} \approx s_iN_{ij}$. This means the parametrization constructs a low rank approximation of the observed count matrix up to a scaling factor.

\subsection{Non-negative matrix factorization}

In this specific case the matrix factorization must be a non-negative matrix factorization (NMF). i.e. no entry in either $\bU$ nor $\bV$ can be lower than 0. In our case, the objective follows from the ML principle stated in Equation \ref{eq:3-nmf-obj}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/nmf-obj.tex}
  \caption{NMF objective.}
  \label{eq:3-nmf-obj}
\end{Equation}

NMF is a class of problems to be considered by itself. It shows up in other problems where the goal is for the model to output exclusively non-negative values. Constraining the entries of the factor to be non-negative is an avenue to so that.

In the general case the objective can also be formulated with the Frobenius norm as shown in Equation \ref{eq:3-nmf-obj-frob}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/nmf-obj-frob.tex}
  \caption{NMF Frobenius loss based objective.}
  \label{eq:3-nmf-obj-frob}
\end{Equation}

Because this problem is intrinsically different from the previously seen matrix factorization, the same approaches do not function. The approaches for NMF is often part-based in nature.

One solution is to use a variant of ALS (see Section \ref{sec:ALS}) where at each alternation is augmented by a projection step that clips the entries to be non-negative.

\section{Embeddings}

The idea of embeddings is to learn a representation of data that carries meaning about the data in a meaningful way. For instance for words, word-embeddings are vector representations which still conserve meaning in some way.

For word embeddings, we consider their meaning to be defined by their use in the language, or their use in a context.

Given a large corpus of words uses in a context, the goal is to construct word representation that carry meaning. i.e. that embed symbols in a vector space e.g. the angles/distance should relate to the meaning of a word. In other words, the goal is to learn a mapping from words $w$ in a vocabulary $\Sigma$ to a vector representation $\bz_w$ in a vector space $\Rm$ usually with high dimensionality $m$, typically 100-500.

The main approach to learn these embeddings is to treat them as latent variables that predict their co-occurring context words (in a fixed-size window).

\subsection{Skip-gram model}

With the data encoded as a sequence of words $\bx = x_1, x_2, \dots, x_T$, the log likelihood under an iid assumption is shown in Equation \ref{eq:3-embed-ll}. Here the interval $\mathcal{I}$ encodes the set of displacement (e.g. for a window of size $2R$: $\mathcal{I} = \{-R,\dots,-1,1,\dots, R\}$). Padding with a special symbol is used for the extremities of $\bx$ 

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-ll.tex}
  \caption{Log likelihood for word embeddings in skip-grams.}
  \label{eq:3-embed-ll}
\end{Equation}

In order to relate the prediction of skip-grams to the latent word embeddings, we use a log-bilinear model. This model is shown in Equation \ref{eq:3-embed-bilin}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-bilin.tex}
  \caption{Log-bilinear model to relate skip-gram prediction to word embeddings.}
  \label{eq:3-embed-bilin}
\end{Equation}

The refinement of this model that is more performant is an augmentation by a bias $b_v$ for each word to control the marginal probability explicitly (i.e. frequent words have high bias), and different embeddings for predicted $\bz$ and conditioned words $\bzeta$. This augmented model is shown in Equation \ref{eq:3-embed-bilin-better} with parameters $w \mapsto (\bz_w, \bzeta_w, b_w) \in \mathbb{R}^{2m+1}$.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-bilin-better.tex}
  \caption{Better log-bilinear model to relate skip-gram prediction to word embeddings.}
  \label{eq:3-embed-bilin-better}
\end{Equation}

With these refinements and modelling of the skip gram the sufficient statistic is the $\vert\Sigma\vert \times \vert\Sigma\vert$ matrix $\bN$ which contains the number of co-occurrences of words in a skip-gram defined by some $\mathcal{I}$. In mathematical terms, the entries of $\bN$ are defined rigorously in Equation\ref{eq:3-embed-suff-stat}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-suff-stat.tex}
  \caption{Sufficient statistic for word embeddings.}
  \label{eq:3-embed-suff-stat}
\end{Equation}

The log-likelihood can also be expressed expliciely as shown in Equation \ref{eq:3-embed-ll-explicit}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-ll-explicit.tex}
  \caption{Explicit log-likelihood for word embeddings.}
  \label{eq:3-embed-ll-explicit}
\end{Equation}

The computation of the normalization constant in the log-likelihood ($\ln\sum_{u \in \Sigma} \dots$) is however quite expensive. The solution to this problem is to transform the current prediction problem into a binary classification problem. This approach can be taken here because the goal is not the prediction in the end, only the representation is to be learned, so it is possible to change the task without impacting the goal.

\subsection{Reformulation as logistic regression}

With this change of prespective we can rewrite the probability $P(v\vert w)$ as shown in Equation \ref{eq:3-embed-log-regr}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-log-regr.tex}
  \caption{Reformulation of the problem as logistic regression.}
  \label{eq:3-embed-log-regr}
\end{Equation}

The task for the classification problem is to distinguish the terms that co-occur in the corpus (the multiset $\mathcal{S}^+$  in Equation \ref{eq:3-embed-classes}) and randomly sampled pairs of words from the vocabulary (the multiset $\mathcal{S}^-$  in Equation \ref{eq:3-embed-classes}). For the negative multiset, $q$ is a probability mass function over $\Sigma$ and $r$ is the sampling factor. 

\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-classes.tex}
  \caption{Classes for the classification task.}
  \label{eq:3-embed-classes}
\end{Equation}

With this reformulation of the task, the log-likelihood that results is much easier computationally. It is shown in Equation \ref{eq:3-embed-better-ll}.


\begin{Equation}[H]
  \input{res/3-lvm/eqn/embed-better-ll.tex}
  \caption{Log likelihood for logistic regression.}
  \label{eq:3-embed-better-ll}
\end{Equation}

A good choice for the distribution $q$ is shown in practice to be $q(w) \propto p(w)^\alpha$, where $p(w)$ is the relative frequency of the word, and $\alpha$ typically 3/4. 

\subsection{Reformulation as matrix factorization (GloVe)}

Considering a  different objective shown in Equation \ref{eq:3-glove-obj}.

\begin{Equation}[H]
  \input{res/3-lvm/eqn/glove-obj.tex}
  \caption{GloVe objective function .}
  \label{eq:3-glove-obj}
\end{Equation}

The big advantage to this approach is that it is able to work with un-normalized models because it is two-side constrained, whereas the only thing constraining the previous models is the budget os probability. So we can simply choose $\ln p(v,w) = \left\langle \bzeta_w, \bz_v \right\rangle$. Here $f(N_{vw})$ is a weighting function. 

This approach can be seen as another matrix factorization algorithm. This is easily seen by getting rid of the weighting function in Equation \ref{eq:3-glove-obj}, and noticing $\hat{\bN} = \bU\bV$ where $\bU = [\bzeta_{w1}, \dots, \bzeta_{wn}]$ and $\bV = [\bz_{w1}, \dots, \bz_{wm}]$. The goal is to find $\bU, \bV$ that minimizes Equation \ref{eq:3-glove-obj}. This objective can then be solved by gradient descent.