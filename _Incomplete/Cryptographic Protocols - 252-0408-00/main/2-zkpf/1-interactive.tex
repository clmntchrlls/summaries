\chapter{Interactive proofs}

\section{Introduction}
    In this section we define basic concepts. We also provide intuitions for concepts which will be defined rigorously later.

\begin{definition}[Proof systems]
    For a statement $s \in \mathcal{S} \subseteq \Sigma^\ast$, we define the truth function $\tau$. 
    $\tau$ maps the statements to their truth value (0 if $s$ is false, 1 if true).
    \[
        \tau : \mathcal{S} \mapsto \{0, 1\}
    \]
    An element $p \in \mathcal{P} \subseteq \Sigma^\ast$ is either a valid or invalid proof for the statement $s$ which we model by the verification function $\phi$. $\phi(s,p) = 1$ means the proof is valid for the statement.
    \[
        \phi : \mathcal{S} \times \mathcal{P} \mapsto \{0,1\}
    \] 
    A \textbf{proof system} is the quadruple $\Pi = (\mathcal{S}, \mathcal{P}, \tau, \phi)$.
\end{definition}

\begin{definition}[Soundness of a proof system]
    A proof system $\Pi = (\mathcal{S}, \mathcal{P}, \tau, \phi)$ is sound if no false statement has a proof. i.e. 
    \[
        \forall s\in S  \ \exists p \in P, \ \phi(s, p) = 1 \Longrightarrow \tau(s) = 1 
    \]
\end{definition}

\begin{definition}[Completeness of a proof system]
    A proof system $\Pi = (\mathcal{S}, \mathcal{P}, \tau, \phi)$ is complete if all true statements have a proof. i.e.
    \[
        \forall s \in S, \tau(s) = 1 \Longrightarrow \exists p \in P \ \phi(s,p) = 1
    \]
\end{definition}

\begin{definition}[Efficiency pf a proof system]
    A proof system $\Pi = (\mathcal{S}, \mathcal{P}, \tau, \phi)$ is efficient if $\phi$ is efficiently computable\footnote{We use the computer science notion of complexity to evaluate efficience. Efficient means at most polynomial time}. 
\end{definition}

\begin{example}[Existence of Hamiltonian cycles in a graph]
    in an undirected graph $G = (V, E)$ where $V = \{0,1,2, \dots, n-1\}$ is the vertices and $E \subseteq V \times V$ the edges. Using the adjacency matrix representation the graph is represented with a symmetric $n \times n$ binary matrix $M$ with where $M_{i,j} = 1 \Leftrightarrow (i,j) \in E$, a graph can be represented with a $n^2$ binary string.\\
    A Hamiltonian cycle in a graph is a closed path from node 0 to node 0 following edges between vertices and visiting each vertex exactly once. We are interested in proving a graph $G$ has a Hamiltonian cycle. To prove it, we can simply provide the list of vertices in which the cycle visits. Since numbers can be represented in binary with $\left \lceil \log_2{n} \right \rceil$ bits, we can represent this list with $n\left \lceil \log_2{n} \right \rceil$ bist so $\mathcal{S}, \mathcal{P} \subseteq\{0,1\}^\ast$. \\
    We define $\tau(s) = 1$ if and only if $\vert s \vert = n^2$ for some $n$ and if the $n^2$ bits of $s$ encode the adjacency matrix of a graph containing a Hamiltonian graph. Otherwise $\tau(s) = 0$. \\
    Moreover we define $\phi(s,p) = 1$ if and only if, when interpreting $s$ as an $n \times n$ binary matrix $M$ and interpreting $p$ as a sequence of $n$ numbers $(a_0, a_1, \dots, a_{n-1})$ and 
    \[
        \forall i \in {0,1,\dots, n-1} M_{a_i, a_{i+1}} = 1
    \]
    Otherwise $\phi(s,p) = 0$.\\
    $\phi$ is efficiently computable ($\mathcal{O}(n)$), no false statement has a proof, and every true statement has a proof, thus $\Pi = (\mathcal{S}, \mathcal{P}, \tau, \phi)$ is efficient, complete, and sound.
\end{example}

\begin{remark}[Absence of Hamiltonian cycles in a graph]
    It is interesting to note that the proof for the inexistence of hamiltonian cycles in a graph is thought to be inefficient. For the moment only bruteforcing all possible cycles until all have been tried is known to work and is really inefficient.
\end{remark}

\subsection{Types of proofs}
    Two different types of proofs exist: Proofs of statement where a statement is proven (e.g. the earth is round, 7 is prime, P = NP, etc.) and proofs of knowledge (e.g. I know an isomorphism $\pi$ from $G_0$ to $G_1$, I know the discrete logarithm of $c \mod M$, etc.)\\ A proof of knowledge implies a proof os statement since knowing an isomorphism means there exists one for instance. Proofs of knowledge are formally defined and examined in Section \ref{sec:proof-know}.

\subsection{Interactive proofs}
    So far we were only interested in static proofs, where the proof is a statement. In interactive proofs, the prover $P$ tries to convince a verifier $V$ of a statement by multiple interaction and information exchange. A static proof is also an interactive proof where $P$ simply provides the proof $p$ to $V$.
    \paragraph{Completeness} If the statement $s$ (resp. $P$ knows the information they claim to) then the correct verifier $V$ always accepts the proof given by the correct prover.
    \paragraph{Soundness} If the statement is false (resp. $P$ does not know the information they claim to) then the correct verifier will always accept the proof with negligible probability independently of the prover's strategy.
    \paragraph{Zero-knowledge} As long as the prover follows the protocol, it is impossible for the verifier to gain any information but that the statement is true (resp. $P$ knows the information they claim to). The zero-knowledge property is formally defined and examined in Section \ref{sec:0-know}
    

\begin{example}[Graph isomorphism]
    In this protocol $\texttt{P}$ proves to $\texttt{V}$ that they know an isomorphism between $G$ and $G'$.\\
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$G, G', \sigma: G' = \sigma G \sigma^{-1}$}
                \DontPrintSemicolon
                \SetKwFunction{P}{P}
                \SetKwProg{Prover}{Prover}{:}{}
                \Prover{\P}{
                    Generate random permutation $\pi$\;
                    $\mathcal{T} = \pi G_0 \pi^{-1}$\;
                    $\mathcal{T} \Rrightarrow  \texttt{V}$\;
                    \;
                    $c \Lleftarrow \texttt{V}$\;
                    $c == 0 \ ? \ \rho = \pi : \rho = \pi\sigma^{-1}$\;
                    $\rho \Rrightarrow \texttt{V}$\;
                    \;
                    \;
                    \;
                }
            \end{algr}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$G, G'$}
                \DontPrintSemicolon
                \SetKwFunction{V}{V}
                \SetKwProg{Verifier}{Verifier}{:}{}
                \Verifier{\V}{
                    \;
                    \;
                    $\mathcal{T} \Lleftarrow \texttt{P}$\; 
                    $c \progets \{0,1\}$\;
                    $c \Rrightarrow \texttt{P}$\;
                    \;
                    $\rho \Lleftarrow \texttt{P}$\;
                    $c == 0 \ ?$\;
                    $\quad \mathcal{T} == \rho G \rho^{-1}$\;
                    $\quad : \mathcal{T} == \rho G' \rho^{-1}$
                }
            \end{algr}
        \end{minipage}
        \hfill
    \end{minipage}
\end{example}

\begin{example}[Graph Non-isomorphism]
    In this example, $\texttt{P}$ proves the non-isomorphism of two graphs $G_0$ and $G_1$. Note this proof is valid since it is only requires for the verifier to be efficient.\\
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$G_0, G_1$}
                \DontPrintSemicolon
                \SetKwFunction{P}{P}
                \SetKwProg{Prover}{Prover}{:}{}
                \Prover{\P}{
                    \;
                    \;
                    \;
                    $\mathcal{T} \Lleftarrow \texttt{V}$\;
                    $\mathcal{T} \sim G_0 \ ?\  r = 0$\;
                    $\mathcal{T} \sim G_1 \ ?\  r = 1$\;
                    $r \Rrightarrow \texttt{V}$\;
                    \;
                    \;
                }
            \end{algr}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$G_0, G_1$}
                \DontPrintSemicolon
                \SetKwFunction{V}{V}
                \SetKwProg{Verifier}{Verifier}{:}{}
                \Verifier{\V}{
                    Generate random permutation $\pi$\;
                    $b \progets \{0,1\}$\;
                    $\mathcal{T} = \pi G_b \pi^{-1}$\;
                    $\mathcal{T} \Rrightarrow \texttt{P}$\;
                    \;
                    \;
                    \;
                    $r \Lleftarrow \texttt{P}$\;
                    $r == b$\;
                }
            \end{algr}
        \end{minipage}
        \hfill
    \end{minipage}
\end{example}


\begin{example}[Fiat-Shamir]
    In this protocol $m$ is a RSA modulus. i.e. $m = pq$ for $p$ and $q$ prime. In this protocol, $\texttt{P}$ proves the knowledge of the square root of a number $z$ known to the verifier $\texttt{V}$.\\
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$z \in \mathbb{Z}_{m}^\ast, x: x^2 = z$}
                \DontPrintSemicolon
                \SetKwFunction{P}{P}
                \SetKwProg{Prover}{Prover}{:}{}
                \Prover{\P}{
                    $k \progets \mathbb{Z}_{m}^\ast$\;
                    $t = k^2$\;
                    $t \Rrightarrow \texttt{V}$\;
                    \;
                    $c \Lleftarrow \texttt{V}$\;
                    $r = kx^c$\;
                    $r \Rrightarrow \texttt{V}$\;
                    \;
                }
            \end{algr}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$z \in \mathbb{Z}_{m}^\ast$}
                \DontPrintSemicolon
                \SetKwFunction{V}{V}
                \SetKwProg{Verifier}{Verifier}{:}{}
                \Verifier{\V}{
                    \;
                    \;
                    $t \Lleftarrow \texttt{P}$\;
                    $c \progets \{0,1\}$\;
                    $c \Rrightarrow \texttt{P}$\;
                    \;
                    $r \Lleftarrow \texttt{P}$\;
                    $r^2 == tz^c$\;
                }
            \end{algr}
        \end{minipage}
        \hfill
    \end{minipage}
\end{example}

\begin{example}[Guillou-Quisquater]
    In this protocol $m$ is a RSA modulus. i.e. $m = pq$ for $p$ and $q$ prime. In this protocol, $\texttt{P}$ proves the knowledge of the $e$-th root of a number $z$ known to the verifier $\texttt{V}$.\\
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$z \in \mathbb{Z}_{m}^\ast, x: x^2 = z$}
                \DontPrintSemicolon
                \SetKwFunction{P}{P}
                \SetKwProg{Prover}{Prover}{:}{}
                \Prover{\P}{
                    $k \progets \mathbb{Z}_{m}^\ast$\;
                    $t = k^e$\;
                    $t \Rrightarrow \texttt{V}$\;
                    \;
                    $c \Lleftarrow \texttt{V}$\;
                    $r = kx^c$\;
                    $r \Rrightarrow \texttt{V}$\;
                    \;
                }
            \end{algr}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$z \in \mathbb{Z}_{m}^\ast$}
                \DontPrintSemicolon
                \SetKwFunction{V}{V}
                \SetKwProg{Verifier}{Verifier}{:}{}
                \Verifier{\V}{
                    \;
                    \;
                    $t \Lleftarrow \texttt{P}$\;
                    $c \progets \{0,1, \dots, e-1\}$\;
                    $c \Rrightarrow \texttt{P}$\;
                    \;
                    $r \Lleftarrow \texttt{P}$\;
                    $r^e == tz^c$\;
                }
            \end{algr}
        \end{minipage}
        \hfill
    \end{minipage}
\end{example}


\begin{example}[Schnorr]
    In this protocol, $\texttt{P}$ proves the knowledge of discrete logarithm $x$ of a given $z$ in a cyclic group $H = \langle h \rangle$ of order $q$ known to the verifier $\texttt{V}$.\\
    \begin{minipage}{\textwidth}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$z \in H, x \in \mathbb{Z}_{q} : h^x = z$}
                \DontPrintSemicolon
                \SetKwFunction{P}{P}
                \SetKwProg{Prover}{Prover}{:}{}
                \Prover{\P}{
                    $k \progets \mathbb{Z}_{q}$\;
                    $t = h^k$\;
                    $t \Rrightarrow \texttt{V}$\;
                    \;
                    $c \Lleftarrow \texttt{V}$\;
                    $r = k+xc$\;
                    $r \Rrightarrow \texttt{V}$\;
                    \;
                }
            \end{algr}
        \end{minipage}
        \hfill
        \begin{minipage}[t]{.47\textwidth}
            \begin{algr}[H]
                \caption{$z \in H$}
                \DontPrintSemicolon
                \SetKwFunction{V}{V}
                \SetKwProg{Verifier}{Verifier}{:}{}
                \Verifier{\V}{
                    \;
                    \;
                    $t \Lleftarrow \texttt{P}$\;
                    $c \progets \mathcal{C} \subseteq \mathbb{Z}_q$\;
                    $c \Rrightarrow \texttt{P}$\;
                    \;
                    $r \Lleftarrow \texttt{P}$\;
                    $h^r == tz^c$\;
                }
            \end{algr}
        \end{minipage}
        \hfill
    \end{minipage}
\end{example}


\section{Interactive proofs}
Interactive proofs are a generalisation of non-interactive proofs in which the proof string $p$ is replaced by an unbounded prover algorithm $\texttt{P}$ with which a verifier $\texttt{V}$ can interact. Moreover, with interactive proofs, the verifier is allowed to fail to verify a true or a false statement-prover pair with small probability. Given an input $z \in \{0,1\}^\ast$, \texttt{P} and \texttt{V} interact. At the end on the interaction $V$ outputs a bit.

\begin{definition}[Interactive proofs]
    An interactive proof for a language $L$ is a pair of probabilistic algorithms $(\texttt{P}, \texttt{V})$, where \texttt{V} is efficient such that for all $z \in L$:
    \begin{itemize}
        \item \textbf{Completeness} If $z\in L$, then \texttt{V} accetps the interaction with \texttt{P} with a probability of at least $\frac{3}{4}$
        \item \textbf{Soundness} If $z\notin L$, then \texttt{V} accepts the interaction with any algorithm \texttt{P'} qith probability at most $\frac{1}{2}$.
    \end{itemize}
\end{definition}

\begin{definition}[Complexity class \textbf{IP}]
    \textbf{IP} is the set of languages that can be decided by an interactive proof.
\end{definition}

\begin{definition}[Complexity class \textbf{PSPACE}]
    \textbf{PSPACE} is the set of languages ehich can be decided by an algorithm using a polynomial amount of space.
\end{definition}

\begin{theorem}
    \textbf{PSPACE} = \textbf{IP}
\end{theorem}

Interactive proofs can be used for identification protocols, for digital signatures, or as building blocks for multi-party computation.
