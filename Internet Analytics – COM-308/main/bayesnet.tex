\chapter{Bayesian networks}\label{chap:bayesnet}
Bayesian networks are a simple way to represent the relationships of dependence and independence on a set of random variables. It is also useful to simplify the computations. With 100 binary random variables, if we consider them all independent of each other, there are $2^100$ parameters, however, if we set them up smartly on a Bayesian graph, we can drastically reduce the number of parameters because in the general case $P(AB)$ has $\vert A \vert\vert B\vert$ parameters, if they are independent $P(A)P(B)$ has $\vert A \vert - 1 + \vert B\vert - 1$ parameters, and $P(A\vert B)$ has $\vert B \vert (\vert A\vert -1)$ parameters.\par
For instance, if we have five random variables $A,B,C,D$, and $E$ and $C$ only depends on $A$ and $B$, $D$ depends only on $C$, and $E$ on $B$ we have:
\begin{figure}[!ht]
    \centering
    \begin{tikzpicture}[node distance=2cm]
        \node[draw, circle](A)                    {$A$};
        \node[draw, circle](C) [below right of=A] {$C$};
        \node[draw, circle](B) [above right of=C] {$B$};
        \node[draw, circle](E) [below right of=B] {$E$};
        \node[draw, circle](D) [below of=C]       {$D$};

        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (A) -- (C);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (B) -- (C);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (B) -- (E);
        \draw[decoration={markings,mark=at position 1 with
        {\arrow[scale=2,>=stealth]{>}}},postaction={decorate}] (C) -- (D);
    \end{tikzpicture}
    \caption{Bayesian network example}
    \label{fig:bayesnet-example}
\end{figure}
The graph in Figure \ref{fig:bayesnet-example} allows us to quickly identify the independence relationships. In this case:
\begin{align*}
    P(A,B,C,D,E) &= P(B,C,D,E \vert A) P(A)\\
    &= P(C,D,E \vert A, B)P(B \vert A)P(A) \\
    &= P(C,D,E \vert A, B)P(B)P(A) \\
    &= P(C,D \vert A, B)P(E \vert  A, B)P(B)P(A) \\
    &= P(C,D \vert A, B)P(E \vert B)P(B)P(A)\\
    &= P(D \vert A, B, C) P(C \vert A, B)P(E \vert B)P(B)P(A) \\
    &= P(D \vert C) P(C \vert A, B)P(E \vert B)P(B)P(A) 
\end{align*}
Now is some random variables are observed it is much easier to infer the values of the others than in the fully independent case. The rule to determine two random variables are independent under some condition is that $A \perp ND(A) \vert PA(A)$, where $ND(A)$ are all non-descendents of $A$, and $PA(A)$ are the parents of $A$. Inference can be made top-down: Causal reasoning, bottom-up: Evidential reasoning, and both: Intercausal reasoning.\par
Now that the independence between variables are found, if we observe $Y \subset X$ variables and $Z = X \setminus Y$ and want to infer $P(Z_n \vert Y)$, we need to compute:
\[
    \sum_{Z_1, \dots, Z_{n-1}, Z_{n-2}, \dots} P(Z_1, Z_2, \dots \vert Y)
\]
Which is still very costly, fortunately other methods which are not exact yield good results such Gibbs Sampling.
\section{Gibbs sampling}
Let the Markov chain Q whose states are all possible assignments of the random variables $Z$. We pick a $k$ at random in $\{1,2,\dots, \vert Z \vert\}$ and then sample $Z_k$ from \[
    P(Z_k \vert Z_1, Z_2, \dots, Z_{k-1}, Z_{k+1}, \dots, Y = y )
\]
If we repeat thus process a sufficient number of times, the stationary distribution is $\pi(\mathbf{z}) = P(\mathbf{Z} = \mathbf{z} \vert Y = y)$. This works because the Markov chain is aperiodic, irreducible, and positive-reccurent, thus ergodic.