\chapter{Clustering and communities}
\section{Clustering}
Clustering allows finding organization in data in order to compress the data or denoize data. In image compression, it is important to reduce the set of possible colors by clustering similar colors together in order to represent them with a single one.
\subsection{\textit{K} Means}
$K$-means is an iterative algorithm that finds a reasonably good solution in a finite number of steps while finding the optimal solution is NP-hard. For an input of $N$ data points $\{x_i\}_{i=0}^{n-1}$ and $K$ clusters we find the $K$ centers that minimize the norm squared of the distance between each data point and the closest mean $\mu_k$. The cost function $J$ is:
\[
    J = \sum_{n=0}^{N-1}\sum_{k=0}^{K-1} r_{n,k} \Vert x_n - \mu_k \Vert^2
\]
Where $r_{n,k}$ is the indicator that $k$ is the closest mean to $x_n$. The algorithm to find a local minimum of $J$ is:

\begin{algorithm}[H]
    \caption{$K$-means}\label{alg:k-means}
    \begin{algorithmic}[1]
        \Function{$K$-means}{}
            \State Initialize $\{\mu_k\}_{k=0}^{K-1}$ to random values.
            \While{$J$ decreases}
                \State Assign each data point $\{x_n\}_{n=0}^{N-1}$ to its closest mean.
                \State Updates the means to match the average of the points assigned to it.
                \State Compute $J$.
            \EndWhile
        \EndFunction
    \end{algorithmic}
\end{algorithm}
$K$ means converges in a finite number of steps since $J$ never increases, but no guarantees are given about the globality of the minimum found.
\subsection{Gaussian Mixture Models} Gaussian mixture models (GMMs) assume there exist $K$ clusters, to each of which is associated a distribution of the position a point will take, provided it belongs to this cluster. The goal is to find the covariance matrix and the mean of each of these clusters. The probability distribution of a point $P(X = x)$ is given by $\pi_k$, the probability of the point to belong to one of the $K$ clusters, and by the probability distribution of that cluster $N(x;/mu_k, \Sigma_k)$ and is expressed as
\[
    P(X = x) = \sum_{k=0}^{K-1}\pi_k\mathcal{N}(x; \mu_k, \Sigma_k)
\]
In order to fit the model to the data we also proceed in two steps, we first try to estimate the probability of $x$ to belong to a given cluster $\gamma_{nk} = P(Z_{nk} = 1 \vert x_n)$ (the posterior), where $Z_{nk}$ is the random variable responsible for the assignation of the point to a cluster. (The random variable $Z_{n} = (Z_{n0}, Z_{n1}, \dots, Z_{n(K-1)})$ is one hot and if $Z_{nk} = 1$, then $x_n$ belongs to cluster $k$. $P(Z_n) = \prod_{k}\pi_k^{Z_{nk}}$) 
\[
    \gamma_{nk} = \frac{\pi_k\mathcal{N}(x_n; \mu_k, \Sigma_k)}{\sum_{j}\pi_j \mathcal{N}(x_n; \mu_j, \Sigma_j)}
\]
Then, we use this value to compute estimates for $\mu_k$, $\Sigma_k$, and $\pi_k$. In order to estimate these parameters we use maximum likelihood which yields:
\begin{align*}
    \mu_k &= \frac{1}{N_k}\sum_{n}\gamma_{nk}x_n\\
    \Sigma_k &= \frac{1}{N_k}\sum_{n}\gamma_{nk} \Vert x_n-\mu_k \Vert^2 \\
    \pi_k &= \frac{N_k}{N}
\end{align*}
With $N_k = \sum_{n} \gamma_{nk}$ which is roughly the number of points in cluster $k$.

\begin{algorithm}[H]
    \caption{GMM}\label{alg:gmm}
    \begin{algorithmic}[1]
        \Function{GMM}{}
            \State Initialise all parameters to random values.
            \While{Log likelihood decreases}
                \State Update $\gamma_{nk}$
                \State Update $\mu_{k}$
                \State Update $\Sigma_{k}$
                \State Update $\pi_{k}$
            \EndWhile
        \EndFunction
    \end{algorithmic}
\end{algorithm}

\begin{table}
    \centering
    \begin{tabular}{l|l l}
        & \textit{k}-Means & GMM \\\hline
        Membership & hard & soft \\
        Generative & no & yes \\
        E-step updates & closest mean $r_{n,k}$ & $\gamma_{nk}$ \\
        M-Step updates & $\mu_k$ & $\mu_k, \Sigma_k, \pi_k$ \\
        Convergence & guaranteed & guaranteed \\
        Optimality & not guaranteed & not guaranteed \\
        Characterization & centers, membership & centers, weights, shapes
    \end{tabular}
    \caption{K-means vs. GMM}\label{tab:kmeans-vs-gmm}
\end{table}
\section{Community detection}
Community detection is basically clustering on graphs. In order to detect such communities, modularity $Q$, a metric of the strength of the division into modules introduced. Modularity is the fraction of the edges inside a given community minus the number of edges that would be inside if the edges were distributed uniformly at random between the vertices. Its range varies between $[-\frac{1}{2}, 1]$ and the greater it is, the stronger the community is. If it is 0, then the split into modules is no better than a random split on a $G(n,p)$ graph.
\begin{align*}
    Q &= \frac{1}{2\vert V\vert}\sum_{C_i \in C} \sum_{u,v \in C_i} \left(\mathbb{1}_{(u,v)\in E} - \frac{d_u d_v}{2\vert V \vert}\right) \\
    &= \frac{\sum_{C_i \in C} (m_i - e_i)} {m}\\
    &= \sum_{C_i \in C}\left(\frac{m_i}{m} - \left(\frac{\sum_{u \in C_i} d_u}{2m}\right)^2\right)
\end{align*}
Where $C = \{ C_i\}$ is the set of subsets of $E$ and $C_i \subset E$, $C_i \cap C_j \neq \emptyset \Leftrightarrow i = j$, $m_i$ is the number of edges in $C_i$, and $e_i$ the expected number of edges in $C_i$ is the graph was random computed as in (\ref{eq:exp-edges}). In the inner sum, all oredered pairs of edges are considered $(u,u)$ as well.\par
\begin{equation}\label{eq:exp-edges}
    e_i = \frac{1}{2}\sum_{u,v \in C_i} \frac{d_u d_v}{\vert E \vert}
\end{equation}
Finding the maximum modularity of a graph is NP-hard, but the Louvain method allows to find a reasonably small modularity
\subsection{Louvain method} Louvain builds the communities from the bottom to the top. It starts with all edges in a separate community and then coalesces communities until the modularity no longer decreases