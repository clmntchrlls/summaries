\setcounter{figure}{0}
\setcounter{code}{0}
\setcounter{table}{0}
\setcounter{eqn}{0}

\chapter{Fast Convergence}

\section{Fast Convergence in IP networks}
Convergence is the transition from a routing and forwarding state to another. It is typically triggered by a change in the topology. These changes are either unexpected as when a link failure occurs or planned for maintenance for instance. It is a distributed process during which routers might have inconsistent states. 

Inconsistent routing states can incur traffic losses. Losses are typically due to two different scenarios. When routers route traffic to a down link or node, the traffic is lost (black-holing). The other scenario which can be even worse is the introduction of forwarding loops. In these loops, the packets are endlessly retransmitted in circle until their \texttt{ttl} reaches 0 while clogging the network and using capacity for nothing.

Generally, the transient states do not always lead to blackholes or forwarding loops. In practice, the primary goal during convergence is the reestablishment of connectivity, not
avoiding transient states at all cost. 

There are four main stages for convergence, each of which contribute to some convergence delay. They are all described in detail in the next sections. The first stage is the only local stage, it is the detection of the failure this. All other stages are global and distributed stages propagation of the failure notification, recomputation of the forwarding state, and finally the update of the forwarding state.

The delay caused by all these stages are on the order of the millisecond, but the last stage, the update stage is linear in the number of prefixes and can be on the order of tens of seconds or even minutes. In a simulation with 500'000 prefixes, the convergence time can be up to 2 minutes. Nowadays, the number of prefixes is closer to 1'000'000. Moreover, link and device failures occur with a non negligible frequency as shown in Table \ref{tab:fc-fail-occurrence}\cite{fails} .

\begin{table}[H]
  \centering
  \input{fig/6-fast-convergence/table/fail-occ.tex}
  \caption{Daily failure occurrence \cite{fails}}
  \label{tab:fc-fail-occurrence}
\end{table}


\subsection{Fast detection}
Failure detection can either be performed passively by leveraging the underlying layers properties in some setups or actively through the emission of beacons. In practice, the passive link failure detection is used when possible, complemented with BFD if available, if it is not, fallback on protocol-based active link failure detection. 

\subsubsection{Passive failure detection}

With some physical links, it is possible to rely on physical or link layer signals to detect a failure through lost of light in optical fiber links or loss of carrier signal. The advantage of this approach is the speed at which a failure can be detected (a few milliseconds) and the virtual lack of overhead this approach requires. This type of detection is however not possible on all physical or link layers, for instance it is not available on Ethernet, and it is incapable to detect certain kinds of failures, for instance with optical fiber when the failure occurs after a signal booster on the link.

\subsubsection{Active failure detection}
Failures can also be detected using beacons, keepalive, or hello messages. The general idea is for neighboring routers to periodically exchange messages and mark the link as failed when a certain number $k$ of them are missed in a row. By default, all routing protocols contain a hello protocol for that purpose. These protocols are characterized by two values: the hello interval, the frequency at which the messages are emitted, and the dead interval, the delay after which a link is marked as down when no new hello messages are received. The advantage of this approach is it works on any platform, but also detects a wide range of failures since the link is actually being tested by the protocol. The problem with this approach is the overhead it imposes on the link. For the detection to be fast, the hello frequency must be high which wastes the limited bandwidth.

\subsubsection{Bidirectional Forwarding Detection (BFD)}
In practice, one of the more popular approach is to rely on the hardware to verify the link status. Since these beacons are handled in hardware, their generation and detection is extremely fast. The other advantage of this approach is the software world is oblivious to the beaconning and not impacted by it. When available, this service is exposed to all as Bidirectional Forwarding Detection (BFD). Then the protocols laid on top can simply be notified when a failure occurs. Without the overhead imposed by the protocols, with BFD, the hello interval is typically around 50ms.

One important caveat to note with both BFD and hello-based active failure notification is that these protocols actively test the ability of the link to transmit hello messages, not all possible traffic. It can be the case that one network card is faulty and systemically drops all packets coming from only one special IP prefix because of a memory failure for instance. These failures can be extremely hard to detect. Attempts are being made as extensions of BFD to also transmit a summary of the traffic sent in hello messages the client on the other side can compare with what it received.

\subsection{Fast propagation}
Once the link failure is detected, other nodes in the network must be notified. The propagation of the information must be as quick as possible. The flooding of failure notification messages must begin immediately after a failure, and the notifications must be sent with the highest priority possible.

There might also be cases where it is not necessary for the whole network to learn about the failure for the network to retrieve connectivity.

\subsection{Fast computation}
When the failure is notified, the forwarding state must be recomputed. To do so with OSPF and IGP, the network graph is simply updated to remove the failed link and Dijkstra is run again on the new topology. These algorithms are fast $\mathcal{O}(E\log(V))$ thus no extra mechanisms need to be implemented in that step. Moreover, this process can be made incremental to help scaling further. 

With BGP however this can still be a problem since paths recomputed per prefix and there are lots of prefixes. Moreover, the case can be that a failure occurs on the link to a BGP gateway. Even if there is a secondary gateway to fallback to, if the secondary link has its local preference lower than the failed gateway, nobody in the network knows the alternate route exists except the alternate gateway. In this case, the failure at the main gateway must be advertised, the main BGP gateway must withdraw its advertisements for all prefixes, and the secondary must then advertise the existence of the alternate route for all prefixes before any of the nodes in the network become aware of the existence of the backup path. This process is extremely slow. In order to mitigate that issue, several extensions of BGP exist that allow routers to advertise more than a single path to a prefix within an AS. This allows faster link failure recovery but comes at the cost of larger BGP forwarding tables.

\subsection{Fast updates}
The update of the Forwarding Information Base (FIB) is typically the bottleneck of convergence. Since it is performed per BGP prefix, it is linear in the number of BGP prefixes stored. The total update time for a full BGP table is in the order of minutes. A first mitigation strategy is to prioritize the updates of most used prefixes. 

The long term solution is to this problem is to reorganize the prefix data structure, so it allows fast incremental updates. This is often performed in three steps: the backup state is pre-computed and preloaded in the reorganized data structure then the backup state is simply activated in case of a failure. In IGP, this process is implemented using Loop Free Alternates (LFAs), and for BGP, Prefix-Independent Convergence (PIC) is used.

\subsubsection{Loop-Free Alternates (LFAs)}
In a nutshell a LFA is a neighbor a node can be certain it can forward traffic to in case of a failure while being sure the traffic sent will not be reflected back. Prefix-based and destination-base are the two types of LFAs. 

The advantage of the destination-based LFA is the lower amount of storage and computation required for the LFAs as there are way less nodes than prefixes. Its disadvantage is it needs a prefix independent forwarding protocol such as MPLS to be implemented and running in the network. The advantages and disadvantages of prefix-based are the opposite of the destination-based version.

For instance, in the network topology shown in Figure \ref{fig:fc-lfa}, R1-R3 is a LFA for link R1-R2 from the perspective of R1. This means that when R1-R2 fails, R1 can forward the traffic bound to R2 to R3 through the R1-R3 link knowing R3 will not reflect the traffic back to R1. In contrast, R2-R3 is not a LFA for R2-R1 from the perspective of R2 because if R2 sends traffic bound to R1 through R1-R3, R3 will send the traffic back to R2.

\begin{figure}[H]
  \centering
  \input{fig/6-fast-convergence/tikz/lfa.tex}
  \caption{Simple network topology}
  \label{fig:fc-lfa}
\end{figure}

More formally, a neighbor $N$ can provide a Loop Free Alternate route from $S$ to destination $d$ if and only if the condition in Equation \ref{eqn:fc-lfa-cond} is fulfilled.

\begin{eqn}[H]
  \centering
  \input{fig/6-fast-convergence/eqn/lfa-cond.tex}
  \caption{LFA condition through $N$ from $S$ to $d$}
  \label{eqn:fc-lfa-cond}
\end{eqn}

The algorithm for computing the destination-based LFAs for the set of all the destinations $D$ from a node $x$ is shown in Listing \ref{code:fc-lfa-alg} where the set of neighbors of $x$ are denoted $N$. This algorithm is easily adapted for prefix-based LFAs by swapping destinations for prefixes.

\begin{code}[H]
  \centering
  \input{fig/6-fast-convergence/code/lfa-alg.tex}
  \caption{Pseudocode for the computation of the LFAs from $X$.}
  \label{code:fc-lfa-alg}
\end{code}

Depending on the network topology, it is possible that many or only a few of the link can be protected by an LFA. Networks composed of specific patterns such as triangles lead to better coverage than networks composed of other  specific patterns such as cycles.

\subsubsection{Remote LFAs}
In order to mitigate the lack of LFAs for certain destinations/prefixes, if we consider nodes that are not neighbors as next hops, a LFA is more likely to exist. In a nutshell the idea is to tunnel the traffic to a remote node from which the traffic will be able to resume its journey towards the destination. This approach is very easily implemented using MPLS, but protocols to implement remote LFAs for IP based network also exist.

The algorithm used to compute the remote LFAs $C_{xy}$ on router $x$ for all destinations $y \in Y$can be summarized as follows :
\begin{enumerate}
  \item Let $n$ be the neighbor used by $x$ to reach $y$ normally.
  \item Let $P$ be the set of nodes that $x$ can reach without traversing $(x,n)$.
  \item Let $Q$ be the set of nodes that can reach $y$ without traversing $(x,n)$.
  \item The candidates $C_{xy}$ for remote LFAs are $C_{xy} = Q \cap P$.
\end{enumerate}

The mechanism through which the LFA can be dynamically chosen by the nodes is to have the forwarding table contain not only the primary next hop but also the LFA next hop for each entry (prefix or destination). Then upon making the forwarding decision the state of the link is retrieved from a table or a register and the next hop is chosen accordingly.

\subsubsection{Prefix-Independent Convergence (PIC)}
With the classical implementation of BGP, in the control plane of each of the nodes, each prefix is mapped to its gateway and each gateway mapped to its next hop to reach the gateway. In the classical implementation of the data plane FIB, these tables are merged into a table mapping each prefix to the next hop to reach it. The problem with this approach is the expectedly large amount of entries needing to be updated when a link failure occurs which is on the order of the number of prefixes.

To fix this issue the two-stage table setup is kept in the data plane as well. There are also two tables the first mapping each prefix to its gateway and another mapping each gateway to the next hop to reach it. This way when a failure occurs the number of updates to perform is only of the order of the number of gateways, not the number of prefixes.

\section{Fast Convergence in MPLS networks}
The idea for link protection in MPLS networks is to pre-compute alternate LSP(s) for each primary LSP which are never used until there is a failure on a link composing the primary LSP. 

The alternate LSPs can be implemented in two fashion: end-to-end LSP protection (Section \ref{sec:fc-e2e-lsp}) and local LSP protection (Section \ref{sec:fc-local-lsp}).

\subsection{End-to-End LSP protection}\label{sec:fc-e2e-lsp}
With end-to-end LSP protection an alternate LSP is set up for each primary LSP at the Ingress LER. The Ingress LER can also use RSVP-TE to reserve the alternate path using the Explicit Route Object (ERO). 

The primary and alternate LSPs are all either node- or link-disjoint (NB: node disjoint is the most reliable because node-disjoint $\Rightarrow$ link-disjoint). 

Once a failure occurs on the primary LSP, the node next to the failed link of node sends a \texttt{PathErr} message to the Ingress LER which triggers the fallback to the alternate LSP.

The advantage of the end-to-end variant of LSP protection is that all the traffic is immediately rerouted at the Ingress LER as soon as the Ingress LER receives the \texttt{PathErr} message and this process does not involve any other component of the network. 

Its disadvantages are that one alternate LSP must be setup for all the primary LSPs which doubles the amount of memory necessary at each Ingress LER. The other disadvantage is the relative slowness of the process because it relies on the \texttt{PathErr} message to travel all the way back to the Ingress LER before any action can be taken.

\subsection{Local LSP protection}\label{sec:fc-local-lsp}

In this flavor, the complete source to destination LSP is not relevant. Here all LSRs only maintain one or more alternate route to their neighbors. In other words, each LSR maintains a link-disjoint LSP to all its neighbors.

When a link fails, the traffic to the now disconnected neighbor is simply rerouted through the alternate LSP to the disconnected neighbor.

The advantage of this approach is it that the alternate LSP is used immediately after the failure is detected which is much faster than the \texttt{PathErr} message propagation. 

Its disadvantages are it relies only on link-disjoint LSPs which are less reliable than node-disjoint LSPs, and depending on the structure of the network, many protection LSPs might be necessary.

