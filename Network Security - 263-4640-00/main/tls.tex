\chapter{Transport layer security}
Transport layer security (TLS) was designed as the IETF succcessor of the secure socket layer (SSL) originally developped by Netscape. SSLv1 was broken since its creation, SSLv2 was also seriously flawed and is now deprecated, while SSLv3, vulnerable to the POODLE + RC4 attacks, was translated into the first version of TLS: TLSv1.0. Since TLSv1.0, TLSv1.1 and TLSv1.2 added some security patches and TLSv1.2 added support for AEAD. Some vulnerabilities subsisted through these verisions so TLSv1.3 was redesigned from the ground up.\\
The most notable security issues fixed by TLSv1.3 which are still in TLSv1.2 is the presence of weak cipher suites in the cipher suite negotiaied (e.g. \textit{TLS\_NULL\_WITH}\\\textit{\_NULL\_NULL} i.e. neither encryption nor integrity protection nor authentication). An attacker benefiting from a PtiM position could perform a downgrade attack to achieve MitM for the TLS connection.

\section{Motivation for TLSv1.3}
The goals for TLSv1.3 are to provide a protocol for secure and authentic communication channels, while still being fast. In the TLSv1.3 threat model is an attacker which has the complete control over the network. The only requirements are that the underlying transport layer offers a reliable and in-order data stream. The specific goals are:
\begin{itemize}
    \item \textbf{Entity authentication} \\
    The server is \textbf{always} authenticated and the client is optionally authenticated. This authentication is made by the use of asymmetric cryptography.
    \item \textbf{Confidentiality} \\
    The transmitted data is only visible to the endpoints although TLSv1.3 does not mask the length of the transmitted data, but it offers data padding.
    \item \textbf{Integrity} \\
    Data transmitted cannot be modified in transit without being detected. This covers reordering, insertion, and deletion of data.
    \item \textbf{Efficiency} \\
    The crypto overhead is minimised. It maximises the use of symmetric cryptography and reduces to the minimal number of RTTs for the connection establishment. 
    \item \textbf{Flexibility} \\
    Several authentication and encryption methods are available.
    \item \textbf{Self-negotiation} \\
    The selection of protocol version and of cipher suite is done as part of the protocol.
    \item \textbf{Protection of negotiation} \\
    Negotiation of the protocol is protected to avoid downgrade attacks by an adversary having a MitM position. In particular, the cipher suite selection is protected.
\end{itemize}

\section{TLSv1.3 Record protocol}

\subsection{Records}

During the exchange of data after the connection is established, the protocols data units are called records. The record protocol is thus the protocol handling the transmission of a datastream. TLSv1.3 guarantees data origin authentication and integrity using a MAC; secrecy using symmetric cryptography; prevention of replay, reordering, and deletion by a sequence number protected by the MAC; simulataneous encryption and integrity protection by usind AEAD; and prevention of reflection attacks using two different symmetric keys for the two directions of the traffic. 

\begin{figure}[!ht]
    \centering
    \fig{fig/tls/record.png}
    \caption{Composition of a TLSv1.3 record}
    \label{fig:tls-record}
\end{figure}

Figure \ref{fig:tls-record} shows the composition of a TLSv1.3 record. The payload is a fragment of the datastream to be transmitted, the \textit{ctype} field indicates whether the record is a record a handshake message, an alert, or application data (NB: the unencrypted \textit{otype} field is filled with a dummy value to hinder traffic analysis). Padding is optionnal since it is not needed by encryption and is composed of \texttt{0x00} bytes after the nonzero \textit{ctype} field. The padding is removed after the decryption to prevent padding oracle attacks. \\
The AEAD nonce is constructed from the IV xorred with the sequence number (SQN), which is incremented after each recodr is sent. The IV is a fixed pseudorandom value for each connection which is derived from secrets in the handshake.\\
The record header contains a dummy value in the \textit{otype} field. The version field is also filled with a dummy value for compatibility reasons with routers and firewalls.

\subsection{Security features}
The available ciphers in TLSv1.3 are AES-GCM or AES-CCM with a key of 128 or 256 bits; and 
ChaCha20Poly1305. Decryption errors are treated a fatal: the connection is teared down and the key material discarded. The keys are also negotiated every $2^{24.5}$ records to accomodate with the AES theoretical data limits.\\
Some attacks are however not prevented by TLSv1.3 such as truncation of the stream of records; applicaiton-layer confusion: the records limits do not necessarily corresponds to the application data units; and nothing is done against timing attacks on the padding.

\section{TLSv1.3 Handshake protocol}
In TLSv1.3 the handshake takes only 1 RTT (versus 2 for TLSv1.2) and can only be done in 0 RTT for resuming a connection. {fig:tls-handshake}. \\
The 1 RTT handshake, shown in Figure \ref{fig:tls-handshake}, is achieved by reducing the number of available (EC)DHE groups from which the client sends several for attempting the key exchange. The server picks one and replies with its share and can already redive and use the keys. Most of the handshake traffic is encrypted with keys wich are derived from the exchange, but are different than the record keys. The elements of the groups and curves selected in TLSv1.3 are standardized in  RFC 7919, RFC 4492, and RFC 7748. The handshake is also integrity protected. Because ephemeral key exchange is used, the subsequent exchange of data is forward secure. The key separation policy of TLSv1.3 provides a much greater separation level than TLSv1.2.

\begin{figure}[!ht]
    \centering
    \fig{fig/tls/handshake.png}
    \caption{The TLSv1.3 default handshake. '\texttt{+}' indicates an addition to the message sent, '\texttt{*}' denotes optional or situation dependent data, and '[$\cdot$]' and '$\{\cdot\}$' indicates the data is encrypted.}
    \label{fig:tls-handshake}
\end{figure}

Although all the security measures put in place in TLSv1.3, the handshake protocol still relies heavily on the randomness produced by both the client. If, by observing the first nonce exchanged by the client and the server, an attacker can predict the other random numbers generates, they are in position to eavesdrop the communication.

\section{TLSv1.3 resumption and 0-RTT handshake}
One of the secrets created by the key derivation is the resumption master secret. It allows for the server and the client to achieve resumption handshakes. After the regular handshake is completed, the server can send an additional message: the \texttt{NewSessionTicket}. This ticket contains the pre-shared key identity and, the length for which it can be used, and a nonce (to create multiple identities with a resumption master secret). This ticket can then be used in a later exchange to achieve faster handshakes since it reduces the message sizes and avoids using asymmetric cryptography. In the resumption key exchange, the client sends a list of identities to the server instead of the set of key shares. The authentication is here made by the possession of the resumption master secret, not by asymmetric cryptography.\\
The 0 RTT handshake is achieved in a similar way. On the first message sent by the client, the client simply sends data encrypted with their pre shared key. to which the server can answer with the same key. This feature is controversial since the first data exchange is not forward secure and can potentially be replayed in some circumstances.
