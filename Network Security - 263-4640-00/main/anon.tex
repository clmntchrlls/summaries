\chapter{Anonymous communication systems}

While TLS provides secrecy and authentication, it provides confidentiality only for the data exchanged, not for the metadata. Since the identities of the parties are not obfuscated, it is trivial for an observer of the traffic who talks to whom, when, for how long, etc. In the case of reporting or whistleblowing for instance, it may be desirable to hides the source identity from the destination. \\
This feature is not only useful for crime, but can also be largely used in military applications, in trade and R\&D, by whistleblowers, activists or simply as building blocks for other technologies (such as electronic voting or cryptocurrencies for instance).

\section{Anonymity}
First, Anonymity cannot be defined as a property for one subject or objects, but rather a property over a set of actors or objects. Thus, we define the anonymity set to be the set of all possible subjects (resp. objects) who could have taken part in a given action. The level of anonymity thus depends on the number of other subjects (resp. objects) present in the anonymity set. The smaller the set, the less anonymized the action is, and reciprocally.

\paragraph{Sender anonymity} In this setup the adversary knows the destination but is unable to identify the source amongst the ones in anonymity set. In this setup the sender provides a way for the receiver to reply using a token. The sender anonymity set is all other senders which are indistinguishable from the true sender.  

\paragraph{Receiver anonymity} This setup is the reverse from the previous, here the sender can be identified by the adversary, but the receiver cannot be. The receiver anonymity set is composed of all possible receivers indistinguishable from the true receiver. In this setup, the receiver provides a way for the sender to reach them by the use of a known pseudonym.

\paragraph{Unlinkability} This property guarantees that, even if the sender knows the sender set and the receiver set, it is impossible for them to link a sender and receiver together. In order to achieve this feat the receiver and sender sets must not be composed of a single entity.

\paragraph{Unobservability} This property guarantees an attacker cannot determine whether a communication took place or not. In wireless communication this can be achieved by using DSSS (although some physical limitations exist) and by always sending traffic in wired setups. Unobservability guarantees anonymity.

\paragraph{Plausible deniability} Although this property does not guarantee a strict anonymity, it guarantees the attacker cannot prove a given subject is responsible for a given action. Anonymity guarantees plausible deniability.

\subsection{Threat model}
Many types of adversary need to be considered according to the degree of control of the network used (global vs. local), the type of control they have (direct vs. via a compromise), and their behavior (passive vs. active). This is often not detailed in the specifications and claims.

\section{Basics}
Several solutions can be used for achieving some level of anonymity depending on the network type (wired vs. wireless) or the trustworthiness of other actors.

\subsection{Broadcast}
In wireless networks, the easiest way to achieve receiver anonymity is broadcasting the signal. This however does not guarantee anything for the sender who can be easily de-anonynised via triangulation. If the receiver is known, the sender can even use DSSS to hide the transmission in the noise, thus gaining some level of anonymity. Note however the sender anonymity cannot be guaranteed because of physical properties of radios which make even DSSS signals transmissions detectable.\\
An alternative to DSSS can be to use a burner phone or a hijacked Wi-Fi connection, which achieves the decorrelation between the equipment ID and the identity of the true sender.

\subsection{Proxy}
A naive option would be to use proxies or VPNs to hide the sender identity to the destination and hide the destination to a local adversary. Together with some kind of encryption, the communicated data even stays secret for the proxy. This approach however has a big weakness, The proxy is a single point of failure and if the adversary somehow controls the proxy, all anonymity is lost. 

\subsection{Cascade of proxies}
The solution to the problem posed by the use of a single proxy or VPN is to use a cascade of them. By doing so, the single point of failure problem is removed if the proxies only know the previous and next hop. With three hops or more, sender anonymity is guaranteed for an adversary at the destination and receiver anonymity is guaranteed for an adversary at the source. This guarantee is given when the message traverses at least one honest proxy.

\section{Circuit-based anonymity networks}
Circuit-based anonymity networks are based on the cascade of proxies solution and use a layered encryption approach. These systems are made to support web browsing at the cost of an increased risk of deanonymization: they only work with a local adversary threat model. If an adversary control both ends of the circuit, deanonymization is trivially achievable through traffic analysis.\\
These systems are also known as \textit{onion routing systems}. The circuits are composed of nodes which are called relays. The first node from the source is called the \textit{entry guard}, the second the \textit{middle relay}, and the last before the destination the \textit{exit relay}. The whole circuit is also referred to as the \textit{tunnel}, especially if it is at layer 3. 

\subsection{Circuit lifecycle}
First, before communication can take place, the circuit is set up. In this step, the sender uses the public keys of the relays to create shared keys and both the relays and the client store this state.\\
Then, data is exchanges between the source and the destination through the established circuit. The encapsulated data is encrypted as it would usually be for normal web browsing, but a layer of encryption between the source and the relays is applied. Only symmetric key cryptography is used at this step.\\
Finally, once the communication is over, the circuit is torn down to free the state on the relays and/or to prevent future attacks.

\subsection{Circuit setup}
Two main way  exist for the setup of the circuit: the direct circuit setup, and the telescopic circuit setup. The direct circuit setup is faster to perform, but it lacks perfect forward security and can only guarantee eventual forward security, while the telescopic circuit setup guarantees perfect forward security, but requires more time and messages.

\subsubsection{Direct circuit setup}
When the circuit needs to be setup by a client, the state on relays are established using a single round trip. The packet sent is formed the following way: 
\[
    E_{1asc}\biggl[r_2, g^{x_1}, E_{2asc}\bigl[r_3, g^{x_2}, E_{3asc}[g^{x_3}]\bigr]\biggr]
\]
With $E_{nasc}$ is the encryption with the public key of the $n$-th relay of the enclosed data, $r_n$ is the address of the $n$-th relay, and $g^{x}$ is a DH share.\\
Although this method uses asymmetric cryptography, it is relatively quick since it takes only 1 RTT for the circuit to be established. \\
However, this gain in performance is at the cost of perfect forward security for the circuit. Even if the circuit setup establishes ephemeral keys between the client and the relays which are perfect forward secure, the establishment of the circuit contains the addresses of the relays encrypted with the public keys of the relays. If the setup is recorded and later the private key of a relay leaks, the adversary can replay the all the setup packets and thus discover which of the packets correspond to a given circuit. Thus, the anonymity is broken.\\
A small mitigation to this problem can be found by frequently rotating the asymmetric keys of the relays, but this only provides eventual forward secrecy.

\subsubsection{Telescopic circuit setup}\label{sec:anon-telesc}
In order to fix the shortcomings of the direct circuit setup, telescopic circuit setup can be used at the price of an increased number of RTTs necessary to establish the circuit. In telescopic circuit setup, the client first establishes symmetric keys with the first relay: $ E_{1asc}[g^{x_1}] $. Then the client uses this ephemeral key just derived to contact the second relay and set up the keys: $E_{1sym}\bigl[r_2, E_{2asc}[g^{x_2}]\bigr]$. Finally, the client uses the symmetric key with the first second relay and establishes a symmetric with the third relay:
\[
    E_{1sym}\biggl[r_2, E_{2sym}\bigl[r_3, E_{3asc}[g^{x_3}]\bigr]\biggr]
\]
Where $E_{nasc}$ is the encryption with the public key of the $n$-th relay of the enclosed data, $E_{nsym}$ is the encryption with the symmetric ephemeral key setup with the $n$-th relay of the enclosed data, $r_n$ is the address of the $n$-th relay, and $g^{x}$ is a DH share.\\
This approach sacrifices the circuit establishment speed ($O($number of relays$)$) RTTs for the immediate perfect forward secrecy of the circuit.

\subsection{Data forwarding}
Once the circuit is set up it can be used to transfer a payload to any destination. To do so, the packet sent by the client is formed as follows:
\[
    E_{1sym}\biggl[r_2, E_{2sym}\bigl[r_3, E_{3sym}[d, \text{payload}]\bigr]\biggr]
\]
Where $E_{nsym}$ is the encryption with the symmetric ephemeral key setup with the $n$-th relay of the enclosed data, $r_n$ is the address of the $n$-th relay, and $d$ is the address of the destination.\\
The payload can itself be encrypted using TLS for instance for web browsing. When the destination replies, the packet arrives at the third node which encrypts the payload with the symmetric key it shares with the client and forwards it to the second, etc. 

\subsection{Circuit teardown}
The circuit can be torn down by any member of the circuit. If the client no longer needs the tunnel, it can trigger a teardown. Otherwise, if any relay sense an attack or receives corrupt packets, they can initiate the tear down as well. Circuits have a limited lifetime so must be eventually be torn down.

\subsection{Attacks}
Many attacks against circuit based anonymity networks have been proposed. For many it is unclear whether they fit the threat model defined by the circuit based anonymity networks. Some are practical with only a limited amount of resources while others are only achievable by state-level adversaries.

\subsubsection{Passive traffic analysis}
When the adversary has the ability to observe both ends of the tunnel (not necessarily the entry guard and the exit relay), they can analyze the traffic pattern (timings, size of packets, flow length, etc.) to correlate between the traffic which enters the circuit and the traffic that exits the circuit. Real-time traffic analysis is challenging, but if the adversary can store the data for later analysis, deanonymization is possible.

\subsubsection{Active traffic analysis}
When the adversary has control over the entering and exiting traffic of the circuit and can modify the traffic, the adversary can actively control the inter-packet timings, reorder the packets, drop the packets (though this is detectable), add one bit of information in the traffic (flow watermarking), or event add multiple bits of information (for instance the senders IP) (flow fingerprinting).

\subsubsection{Website fingerprinting}
If the adversary only sits between the client and the circuit and has previously acquired a database of the fingerprints of various websites, it is possible for the adversary to recognize which website the user visits despite them using an anonymizing network. This is particularly effective for interactive applications \cite{chen2010side}.

\subsubsection{Traffic analysis resistance}
In order to defeat traffic analysis attacks on anonymity, it has been proposed to add cover traffic and mixing, but these come with a significant overhead and challenges scalability. This approach has only been suitable for some VoIP application with low bandwidth \cite{le2015herd}. In order to really be secure only a restricted set of flow durations and bandwidth combinations are possible.

\subsubsection{Higher-layer attacks}
Since the data transmitted usually involves higher layers protocols, it is possible to exploit these in order to achieve deanonymization. It is for instance trivial for an adversary at the end of the circuit to probe the TCP stack for instance. One fix for this problem would be to set up one TCP connection per hop, but this only fixes a part of the problem further since the upper layers can also help to deanonymize.\\
TLS or HTTP contain various identifiers that can link a single piece of traffic to its sender: \url{https://amiunique.org}.

\section{Mix-nets}
Mix-nets provide another flavor of anonymity which still works in the presence of an adversary having observation capabilities over the network globally. They work by having multiple clients using them to transfer data to multiple destinations, buffering the messages, reordering them, and forwarding them in batches.\\
A client thus sends the payload they wish to transmit to a destination with address $d$ to the mix-net proxy. The client encrypts the payload and the final destination with the public key of the proxy $E_{pub}$. The message sent to the proxy is the following: $E_{pub}[d, \text{payload}]$.\\
Upon receiving a message to forward, the proxy decrypts the data and stores it. At some later point in time, the proxy sends all the messages it received to their final destination.\\
This method is efficient if the incoming and out coming messages are indistinguishable. Thus padding is necessary to achieve a fixed length of the messages. Moreover, if the sender requires an answer from the destination, they must include a token in their initial message which allows the proxy to forward them the answer from the destination\cite{danezis2009sphinx}.

\subsection{Attacks}
Often, the users only communicate with a subset of the other users, thus, by observing multiple messages being sent from a given user and the destinations of all the messages existing the proxy, it is possible to deanonymize the user. Indeed, all that is necessary to do for the adversary it to compute the intersection between the destinations of the messages existing the proxy at each wave of messages until only one destination remains \cite{danezis2005statistical,kedogan2002limits}.\\
In order to defeat these attacks, cover traffic is needed. Cover traffic is dummy traffic used to increase the size of the anonymity set. This scheme requires the parties to often query the proxy for messages. These queries can either be real queries or contain a message. Once the proxy receives these queries, it returns either a message if one was waiting, or replies with dummy traffic.

\section{Differences between circuit based anonymity networks and mixnets}
The main differences between circuit based anonymity networks (onion routing) and mixnets is summarized in Table \ref{tab:anon-circ-mix-diff}.

\begin{table}[!ht]
    \centering
    \begin{tabular}{l | l | l}
        & Mix-nets & Onion routing \\\hline
        Forwarding system & Message-based & Circuit-based \\
        Layered encryption & Asymmetric & Symmetric \\
        Mixing and batching & Yes & No \\
        Cover traffic & Yes (optionnal) & No\\
        Forward secrecy & No & Yes \\
        Latency & High & Medium \\
        Honesty required by relays & All (only one proxy) & At least one
    \end{tabular}
    \caption{Difference between circuit based anonymity networks and mix-nets}
    \label{tab:anon-circ-mix-diff}
\end{table}

\section{Tor}
The tor project, funded by EFF, was funded in 2006, but takes on the work started in 1981 by Chaum and improved along the way. Tor is now the most widely used anonymous communication system with 2M daily users.

\subsection{Basics}
Tor establishes three relays, uses telescopic circuit setup, per-hop TCP streams, and per-hop TLS. Multiple streams go over the same TLS connection, and end-to-end HTTPS is possible. The favored way to access Tor is through the Tor browser which cleans the HTTP(S) traffic. Tor also supports SOCKS proxies, so any TCP application can make use of a TOR connection. Tor guarantees a secure channel between the client and the exit relay.\\
Tor also provides additional features such as exit policies (an exit can restrict the destinations reachable from it), multiple streams per circuit, censorship resistance through bridges and pluggable transports, and hidden services which also provides receiver anonymity.

\subsection{Tor cell}
The basic unit of traffic in the Tor network is the Tor cell which is a 512B long packet. It contains a circuitId, a command used during circuit setup and teardown, and a payload. The command can be either for circuit establishment or to notify the packet needs to be relayed. The relayed packets, once their payload is decrypted, contain another command (to initiate teardown or to indicate the cell contains data), a stream identifier, a digest, the length of the data, the data, and padding. When receiving the cell the relay checks the digest and proceeds differently whether it is correct or wrong. If it is wrong, it means there are still layers of encryption over the data and the relay is not the final one. Thus, it simply checks the stream id and forwards the cell. If it is right, then the relay is the final relay, thus it checks the command and proceeds accordingly. Only the last relay is able to see the unencrypted transported data.\\
Figure \ref{fig:anon-tor-comm} shows an example of some circuit communication.

\begin{figure}[!ht]
    \centering
    \fig{fig/anon/tor-comm}
    \caption{Example tor communication}
    \label{fig:anon-tor-comm}
\end{figure}

\subsection{Circuit establishment and use}
The circuit is established in a telescopic manner (See section \ref{sec:anon-telesc}). One more feature is enabled however to avoid easy DoS attacks on the relays. If nothing was implemented, nothing would hinder the adversary to create really long chains and then by sending a few packets create a large amount of work for the relays. This is why the circuit establishment is done with special cells \textit{relay\_early} cell. These cells are the only one allowed to contain the command \textit{extend} and only eight of them are allowed per circuit, which caps the maximum path length at 9 hops.

\subsection{Hidden services}
Another feature of Tor are the hidden services which allow for receiver anonymity as well.\\
These service use the hash of their public keys as identifiers which allow the client to verify the identity of the service. In order to set up a way for the clients to be able to reach them. The services establish connections to introduction points (IPs) through tor, which allow for the services to stay anonymous.\\
When a client wants to connect to a hidden service, the client suggests a rendezvous server which will be the meeting point for the client and the service. Finally, both the client and the service connect to the rendezvous point to communicate.

\subsection{Directory authorities}
In order for the clients of the Tor network to know what relays exist, who they are and how to reach them, 10 directory authorities (DA) continually run a consensus to keep the list up to date. This is critical since, if an adversary can provide the list, deanonymization is trivial. The DAs track the states of the relays and store their public keys. The Tor browser comes with a list of the DAs' keys. Normally a client accepts a consensus document if it is signed by the majority of the DAs.\\
These directories are a huge weakness for this protocol since an adversary controlling their majority can compromise the whole network.\\
Every relay in the network periodically reports to the DAs. The DAs also gather performance and stability metrics about the relays. Every hour a new consensus document. In order to prevent Sybil attacks, the DAs limit the number of relays in the same subnet. For scalability reasons, the relays act as a DA cache to lower the need of resources of the DAs.

\subsection{Censorship resistance}
Since the list of relays is public, the access to the Tor network can be very easily censored by blacklisting the IPs of the entry guards. In order to defeat that, Tor keeps a few number of \textit{bridge relays}, which are entry guards not in the DA list. These bridges can be reached by demanding the access to the Tor network or distributed through other networks.\\
Still, the packets Tor clients send are very recognizable and allow the censors to drop them using deep packet inspection. To circumvent this, obfuscation of the traffic was put in place (\textit{pluggable transports}). With this an arms race is in place between the censors and the developers of the various pluggable transports.
